module.exports = {
  HOST: "192.168.0.175",
  USER: "sa",
  PASSWORD: "password_01",
  DB: "testdb",
  dialect: "mssql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
